This project currently lives here: [https://gitlab.com/1f604_public/supersimplewiki](https://gitlab.com/1f604_public/supersimplewiki)

Literally the only reason it's hosted on gitlab is because GitHub doesn't let you fork your own repo and I really need that feature.

# Installation

## Step 1. Install Go

Follow the instructions [here](https://go.dev/doc/install). 

Verify your install by checking that `go version` prints the version of go.

## Step 2. Download this repo and compile hoedown. 

Follow these steps:

```
$ git clone git@github.com:1f604/supersimplewiki.git
$ cd supersimplewiki/dependencies/hoedownv3.08/
$ make
```

Verify that the compilation has succeeded by checking that there is a `hoedown` binary in the `supersimplewiki/dependencies/hoedownv3.08/` directory. Check the version by typing this command:

```
$ ./hoedown --version
```

It should print this:

```
Built with Hoedown 3.0.7.
```

## Step 3. ???

## Step 4. Profit!



















